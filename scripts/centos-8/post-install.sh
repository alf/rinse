#!/bin/sh
#
#  post-install.sh
#  CentOS 8
#  (c) Thomas Lange 2020

prefix=$1
CH="chroot ${prefix}"

if [ ! -d "${prefix}" ]; then
  echo "Serious error - the named directory doesn't exist."
  exit
fi

# rpm's can now be removed
rm -f ${prefix}/*.rpm

# add command, that are normally executed in postinst or similar

# binutils
$CH alternatives --install /usr/bin/ld ld  /usr/bin/ld.bfd 50
$CH alternatives --install /usr/bin/ld ld  /usr/bin/ld.gold 30
$CH alternatives --auto ld
$CH /sbin/ldconfig

# dbus-daemon
$CH /usr/sbin/groupadd -r -g 81 dbus 2>/dev/null || :
$CH /usr/sbin/useradd -c 'System message bus' -u 81 -g 81 -s /sbin/nologin -r -d '/' dbus 2> /dev/null || :

# filesystem
mkdir ${prefix}/usr/lib/debug/usr
mkdir ${prefix}/usr/lib/debug/usr/bin
mkdir ${prefix}/usr/lib/debug/usr/sbin
mkdir ${prefix}/usr/lib/debug/usr/lib
mkdir ${prefix}/usr/lib/debug/usr/lib64
ln -s usr/bin   $prefix/usr/lib/debug/bin
ln -s usr/lib   $prefix/usr/lib/debug/lib
ln -s usr/lib64 $prefix/usr/lib/debug/lib64
ln -s usr/sbin  $prefix/usr/lib/debug/sbin
ln -s ../.dwz   $prefix/usr/lib/debug/usr/.dwz

mv $prefix/var/run/* $prefix/run
rmdir $prefix/var/run
ln -s ../run $prefix/var/run
ln -s ../run/lock $prefix/var/lock
touch ${prefix}/etc/kdump.conf

# libutempter
#$CH groupadd -g 22 -r -f utmp
$CH groupadd -g 35 -r -f utempter

# p11-kit-trust
$CH /usr/sbin/update-alternatives --install /usr/lib64/libnssckbi.so libnssckbi.so.x86_64 /usr/lib64/pkcs11/p11-kit-trust.so 30

# platform-python
$CH alternatives --install /usr/bin/unversioned-python python /usr/libexec/no-python 404 \
    --slave   /usr/share/man/man1/python.1.gz  unversioned-python-man  /usr/share/man/man1/unversioned-python.1.gz

# systemd
$CH groupadd -r -g 22 utmp
$CH groupadd -r input
$CH groupadd -r -g 36 kvm
$CH groupadd -r render
$CH groupadd -r -g 190 systemd-journal
$CH groupadd -r systemd-coredump
$CH useradd -r -l -g systemd-coredump -d / -s /sbin/nologin -c "systemd Core Dumper" systemd-coredump &>/dev/null || :
$CH groupadd -r -g 193 systemd-resolve
$CH useradd -r -u 193 -l -g systemd-resolve -d / -s /sbin/nologin -c "systemd Resolver" systemd-resolve &>/dev/null || :
#$CH setfacl -Rnm g:wheel:rx,d:g:wheel:rx,g:adm:rx,d:g:adm:rx /var/log/journal/

# util-linux
touch ${prefix}/var/log/lastlog
$CH chown root:utmp /var/log/lastlog
chmod 0664 ${prefix}/var/log/lastlog
ln -fs ../proc/self/mounts ${prefix}/etc/mtab

echo "  Updating package"
$CH /usr/bin/dnf -y update

echo "  cleaning up..."
$CH /usr/bin/dnf clean all

umount ${prefix}/proc
umount ${prefix}/sys

echo "  post-install.sh : done."
